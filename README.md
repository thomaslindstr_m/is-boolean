# is-boolean

boolean type checker

```
npm install @amphibian/is-boolean
```

```javascript
var isBoolean = require('@amphibian/is-boolean');

isBoolean(true); // > true
isBoolean('hello'); // > false
```
